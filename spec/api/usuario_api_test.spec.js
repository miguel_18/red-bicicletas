var mongoose = require('mongoose');
var Usuario = require('../../models/usuario');
var request = require('request');
var server = require('../../bin/www');

var base_url = 'http://localhost:3000/api/usuarios';

describe('USUARIO API', () => {
    beforeAll(function (done) {
        var mongoDB = 'mongodb://localhost/testdb'
        mongoose.connect(mongoDB, { useNewUrlParser: true});
    
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'MongoDB connection error: '));
        db.once('open', function () {
            console.log('We are connected to test database!');
            done();
        });

    });

    afterEach(function(done) {
        Usuario.deleteMany({}, function (error, success) {  
            if(error) console.log('error');
            done();
        });   
    });

    describe('GET USUARIOS /', () => {
        it('Status 200', (done) => {
            request.get(base_url, function (error, response, body) {
                var result = JSON.parse(body); 
                expect(response.statusCode).toBe(200);            
                expect(result.usuarios.length).toBe(0);
                done();
            });
        });
    });

    describe("POST USUARIOS /create", ()=> {
        it("Status 200", (done)=> {
            var headers = {'content-type': 'application/json'};
            var user = '{"id":3,"nombre":"Miguel"}';
            request.post({
                headers: headers,
                url: urlServer + '/create',
                body: user
            }, function(error, response, body){
                expect(response.statusCode).toBe(200);
                var usuario = JSON.parse(body).usuario;
                console.log(usuario);
                expect(usuario.nombre).toBe("Miguel");
                done();
            });
        });
    });

    describe('RESERVAR BICI', () => {
        it('Status 200', (done) => {
            var headers = {'content-type': 'application/json'};
            const usuario = new Usuario({ code: 1, nombre: 'Miguel' });
            usuario.save();

            const bicicleta = new Bicicleta({ code: 1, color: 'verde', modelo: 'ruta' });
            bicicleta.save();
            var reserva = '{"usuario":1, ,"desde":"2018-08-03","hasta":"2018-08-04","bicicleta":1}';
            request.post({
                headers: headers,
                url: urlServer + '/reservar',
                body: reserva
            }, function(error, response, body){
                expect(response.statusCode).toBe(200);
                var reserva = JSON.parse(body).reserva;
                console.log(reserva);
                expect(reserva.usuario.nombre).toBe("Miguel");
                done();
            });
        });
    });
});