var mongoose = require('mongoose');
var Bicicleta = require('../../models/bicicleta');
var request = require('request');
var server = require('../../bin/www');

var base_url = 'http://localhost:3000/api/bicicletas';

describe('Bicicleta API', () => {
    beforeAll(function (done) {
        var mongoDB = 'mongodb://localhost/testdb'
        mongoose.connect(mongoDB, { useNewUrlParser: true});
    
        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'MongoDB connection error: '));
        db.once('open', function () {
            console.log('We are connected to test database!');
            done();
        });

    });

    afterEach(function(done) {
        Bicicleta.deleteMany({}, function (error, success) {  
            if(error) console.log('error');
            done();
        });   
    });

    describe('GET BICICLETAS /', () => {
        it('Status 200', (done) => {
            request.get(base_url, function (error, response, body) {
                var result = JSON.parse(body); 
                expect(response.statusCode).toBe(200);            
                expect(result.bicicletas.length).toBe(0);
                done();
            });
        });
    });
    
    describe("POST BICICLETAS /create", ()=> {
        it("Status 200", (done)=> {
            var headers = {'content-type': 'application/json'};
            var aBici = '{"id":3,"color":"verde","model":"ruta","lat":6.852403,"lng":-75.439928}';
            request.post({
                headers: headers,
                url: urlServer + '/create',
                body: aBici
            }, function(error, response, body){
                expect(response.statusCode).toBe(200);
                var bici = JSON.parse(body).bicicleta;
                console.log(bici);
                expect(bici.color).toBe("verde");
                expect(bici.localitation[0]).toBe(6.852403);
                expect(bici.localitation[1]).toBe(-75.439928);
                done();
            });
        });
    });

    describe("UPDATE BICICLETAS /update", ()=> {
        it("Status 200", (done)=> {
            var headers = {'content-type': 'application/json'};
            var aBici = '{"id":3,"color":"verde","model":"ruta","lat":6.852403,"lng":-75.439928}';
            request.post({
                headers: headers,
                url: urlServer + '/update',
                body: aBici
            }, function(error, response, body){
                expect(response.statusCode).toBe(200);
                var bici = JSON.parse(body).bicicleta;
                console.log(bici);
                expect(bici.color).toBe("verde");
                expect(bici.localitation[0]).toBe(6.852403);
                expect(bici.localitation[1]).toBe(-75.439928);
                done();
            });
        });
    });

    describe("DELETE BICICLETAS /delete", ()=> {
        it("Status 200", (done)=> {
            var headers = {'content-type': 'application/json'};
            var aBici = '{"id":3,"color":"verde","model":"ruta","lat":6.852403,"lng":-75.439928}';
            request.delete({
                headers: headers,
                url: urlServer + '/delete',
                body: aBici
            }, function(error, response, body){
                expect(response.statusCode).toBe(200);
                done();
            });
        });
    });
});

/*
describe("Bicicleta API", () => {
    describe("GET BICICLETAS /", () =>{
        it("Status 200", () => {
            expect(Bicicleta.allBicis.length).toBe(0);

            var a = new Bicicleta(1, 'rojo', 'urbana', [-34.6012424,-58.3861497]);
            Bicicleta.add(a);

            request.get("http://localhost:3000/API/bicicletas", function (error, response, body){
                expect(response.statusCode).toBe(200);
            });
        })
    });
});

describe("POST BICICLETAS /create", () => {
    it("Status 200", (done) => {
        var headers = {'Content-type':'application/json'};
        var aBici = '{"id": 10, "color":"rojo", "modelo":"urbana", "lat":-34, "lng":-54 }';
        request.post({
            headers: headers,
            url: 'http://localhost:3000/API/bicicletas/create',
            body: aBici
        },function(error,response,body){
            expect(response.statusCode).toBe(200);
            expect(Bicicleta.findById(10).color).toBe("rojo");
            done();
        });
    });
});

describe("UPDATE BICICLETAS /update", () => {
    it("Status 200", (done) => {
        var headers = {'Content-type':'application/json'};
        var aBici = '{"id": 10, "color":"rojo", "modelo":"urbana", "lat":-34, "lng":-54 }';
        request.post({
            headers: headers,
            url: 'http://localhost:3000/API/bicicletas/update',
            body: aBici
        },function(error,response,body){
            expect(response.statusCode).toBe(200);
            done();
        });
    });
});

describe("DELETE BICICLETAS /delete", () => {
    it("Status 200", (done) => {
        var headers = {'Content-type':'application/json'};
        var aBici = '{"id": 10, "color":"blanco", "modelo":"urbana", "lat":-34, "lng":-54 }';
        request.delete({
            headers: headers,
            url: 'http://localhost:3000/API/bicicletas/delete',
            body: aBici
        },function(error,response,body){
            expect(response.statusCode).toBe(200);
            done();
        });
    });
});
*/