var Bicicleta = require("../../models/bicicleta");
var mongoose = require('mongoose');
describe('Testing Bicicletas', function(){
    beforeAll(function(done){
        var mongoDB = 'mongodb://localhost/testdb'
        mongoose.connect(mongoDB, { useNewUrlParser: true});

        const db = mongoose.connection;
        db.on('error', console.error.bind(console, 'connection error'));
        db.once('open', function(){
            console.log('We are connected to test DB!');
            done();
        });
    });

    afterEach(function(done){
        Bicicleta.deleteMany({}, function(err, success){
            if (err) console.log(err);
            done(); 
        });
    });

    describe('Bicicleta.createInstance', () => {
        it('crea una instancia de Bicicleta', () => {
          var bici = Bicicleta.createInstance(1,"verde","urbana", [-34.54447, -75.16049]);
    
          expect(bici.code).toBe(1);
          expect(bici.color).toBe("verde");
          expect(bici.modelo).toBe("urbana");
          expect(bici.ubicacion[0]).toEqual(-34.54447);
          expect(bici.ubicacion[1]).toEqual(-75.16049);
        });
    });

    describe('Bicicleta.allBicis', () => {
        it('comienza vacia', (done)  => {
            Bicicleta.allBicis(function(err, bicis){
                expect(bicis.length).toBe(0);
                done();
            });
        });
    });

    describe('Bicicleta.add', () => {
        it('agrega solo una bici', (done) => {
            var aBici = new Bicicleta({code: 1, color:"verde", modelo:"urbana"});
            Bicicleta.add(aBici, function(err, newBici){
                if ( err ) console.log(err);
                Bicicleta.allBicis(function(err, bicis){
                    expect(bicis.length).toEqual(1);
                    expect(bicis[0].code).toEqual(aBici.code);
                    done();
                });
            });
        });    
    });

    describe('Bicicleta.findByCode',() => {
        it('devuleve la bici con code 1', (done) => {
            Bicicleta.allBicis(function (err, bicis) {  
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({code: 1, color: "verde", modelo: "urbano"});
                Bicicleta.add(aBici, function(err, newBici){
                    if (err) console.log(err);

                    var aBici2 = new Bicicleta({code: 2, color: "blanca", modelo: "urbana"});
                    Bicicleta.add(aBici2, function(err, newBici){
                        if (err) console.log(err);
                        Bicicleta.findByCode(1,function(error, targetBici){
                            expect(targetBici.code).toBe(aBici.code);
                            expect(targetBici.color).toBe(aBici.color);
                            expect(targetBici.modelo).toBe(aBici.modelo);
                            done();
                        }); 
                    });
                });   
            })   
        });        
    });

    describe('Bicicleta.removeByCode',() => {
        it('elimina la bici con code 1', (done) => {
            Bicicleta.allBicis(function (err, bicis) { 
                expect(bicis.length).toBe(0);

                var aBici = new Bicicleta({code: 1, color: "verde", modelo: "urbano"});
                Bicicleta.add(aBici, function(err, newBici){
                    if (err) console.log(err);

                    var aBici2 = new Bicicleta({code: 2, color: "blanca", modelo: "urbana"});
                    Bicicleta.add(aBici2, function(err, newBici){
                        if (err) console.log(err);

                        Bicicleta.allBicis(function (err, bicis) {
                            expect(bicis.length).toBe(2);

                            Bicicleta.removeByCode(1,function(err,targetBici){
                                if (err) console.log(err);

                                Bicicleta.allBicis(function (err,bicis){
                                    expect(bicis.length).toBe(1);
                                    done(); 
                                })
                                   
                            }); 
                        }); 
                    });
                });   
            })   
        });        
    });

});
/*
beforeEach(() => { Bicicleta.allBicis = []; })
describe("Bicicleta.allBicis", () => {
    it("Comienza vacía", () =>{
        expect(Bicicleta.allBicis.length).toBe(0);
    });
})

describe("Bicicleta.add", () => {
    it("Agregamos una", () =>{
        expect(Bicicleta.allBicis.length).toBe(0);

        var a = new Bicicleta(1, 'rojo', 'urbana', [-34.6012424,-58.3861497]);
        Bicicleta.add(a);

        expect(Bicicleta.allBicis.length).toBe(1);
        expect(Bicicleta.allBicis[0]).toBe(a);
    });
});

describe("Bicicleta.findById", () => {
    it("Encontrar la bici con id", () =>{
        expect(Bicicleta.allBicis.length).toBe(0);

        var a = new Bicicleta(1, 'rojo', 'urbana', [-34.6012424,-58.3861497]);
        var b = new Bicicleta(2, 'blanca', 'urbana', [-34.6012424,-58.3861497]);
        
        Bicicleta.add(a);
        Bicicleta.add(b);

        var biciAux = Bicicleta.findById(a.id)

        expect(biciAux.id).toBe(1);
        expect(biciAux.color).toBe(a.color);
    });
});

describe("Bicicleta.removeById", () => {
    it("Eliminar la bici con id", () =>{
        expect(Bicicleta.allBicis.length).toBe(0);

        var a = new Bicicleta(1, 'rojo', 'urbana', [-34.6012424,-58.3861497]);
        var b = new Bicicleta(2, 'blanca', 'urbana', [-34.6012424,-58.3861497]);
        
        Bicicleta.add(a);
        Bicicleta.add(b);

        Bicicleta.removeById(b.id);

        expect(Bicicleta.allBicis.length).toBe(1);
    });
});*/