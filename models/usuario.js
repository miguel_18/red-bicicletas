var mongoose = require('mongoose');
var Reserva = require('./reserva');
const bcrypt = require('bcrypt');
const crypto = require('crypto');
const uniqueValidator = require('mongoose-unique-validator');
const Token = require('../models/token');
const mailer = require('../mailer/mailer');
const saltRounds = 10;
var Schema = mongoose.Schema;



const validateEmail = function (email) {
    const re = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    return re.test(email)
};

var usuarioSchema = new Schema({
    nombre: {
        type: String,
        trim: true,
        required: [true, 'Este campo es obligatorio']
    },
    email: {
        type: String,
        trim: true,
        required: [true, 'Este campo es obligatorio'],
        lowercase: true,
        unique: true,
        validate: [validateEmail, 'Por favor ingrese un correo válido'],
        match: [/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/]
    },
    password: {
        type: String,
        required: [true, 'Este campo es obligatorio']
    },
    passwordResetToken: String,
    passwordResetTokenExpires: Date,
    verificado: {
        type: Boolean,
        default: false
    },
    googleId: String,
    facebookId: String
});

usuarioSchema.plugin(uniqueValidator,{message: 'El {PATH} ya existe con otro usuario'})

usuarioSchema.pre('save', function (next) {
    if (this.isModified('password')) {
        this.password = bcrypt.hashSync(this.password, saltRounds);
    }
    next();
});

usuarioSchema.methods.validPassword = function (password) {
    return bcrypt.compareSync(password, this.password);
}


usuarioSchema.methods.reservar = function (biciId, desde, hasta, cb) {  
    var reserva = new Reserva({usuario: this._id, bicicleta: biciId, desde: desde, hasta: hasta});
    console.log(reserva);
    reserva.save(cb);  
}

usuarioSchema.methods.enviar_email_bienvenida = function(cb){
    const token = new Token({_userId:this.id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;
    token.save(function(err){
        if(err){ return console.log(err.message); }

        const mailOptions = {
            from : 'no-reply@redbicicletas.com',
            to: email_destination,
            subject: 'Verificacion de cuenta',
            text: 'Hola,\n\n' + 'Por favor, para verificar su cuenta haga click en este link:\n\n' +
                    'http://localhost:3000' + '\/token/confirmation\/' + token.token,
            html: 'Hola,<br><br>' + 'Por favor, para verificar su cuenta haga click en este link:<br><br>' +
            '<a href="' + 'http://localhost:3000' + '\/token/confirmation\/' + token.token + 
            '" target="_blank">Activar Usuario</a>.<br>'        };
        mailer.sendMail(mailOptions,function(err){
            if (err){ return console.log(err.message); }

            console.log('Se envio un email de bienvenida a ' + email_destination);
        });
    });
}

usuarioSchema.methods.resetPassword = function(cb){
    const token = new Token({_userId:this.id, token: crypto.randomBytes(16).toString('hex')});
    const email_destination = this.email;
    token.save(function(err){
        if(err) { return cd(err); }

        const mailOptions = {
            from : 'noreply@redbicicletas.com',
            to: email_destination,
            subject : 'Reset del password',
            text: 'Hola\n\n' + 'Para el reseteo del password clickee en el sgte. enlace: \n' + 'http://localhost:3000' + '\/resetPassword\/' + token.token + '\n'
        };

        mailer.sendMail(mailOptions, function(err){
            if(err){ return cb(err); }

            console.log('Se envio un email para resetear el password al: '+ email_destination );
        });

        cb(null);
    });
}

usuarioSchema.statics.findOneOrCreateByGoogle = function findOneOrCreate(condition, callback){
    const self = this;
    console.log(condition);
    self.findOne({
        $or:[
            {'googleId': condition.id},{'email':condition.email[0].value}
        ]},(err, result) => {
            if(result){
                callback(err,result)
            }else {
                console.log('CONDITION');
                console.log(condition);
                let values = {};
                values.googleId = condition.id;
                values.email = condition.emails[0].value;
                values.nombre = condition.displayName || 'SIN NOMBRE';
                values.verificado = true;
                values.password = condition._json.etag;
                console.log('VALUES');
                console.log(values);
                self.create(values, (err,result) =>{
                    if(err) {console.log(err); }
                    return callback(err,result)
                })

            }
    })
};

usuarioSchema.statics.findOneOrCreateByFacebook = function findOneOrCreate(condition, callback){
    const self = this;
    console.log(condition);
    self.findOne({
        $or:[
            {'facebookId': condition.id},{'email':condition.email[0].value}
        ]},(err, result) => {
            if(result){
                callback(err,result)
            }else {
                console.log('CONDITION');
                console.log(condition);
                let values = {};
                values.facebookId = condition.id;
                values.email = condition.emails[0].value;
                values.nombre = condition.displayName || 'SIN NOMBRE';
                values.verificado = true;
                values.password = crypto.randomBytes(16).toString('hex');
                console.log('VALUES');
                console.log(values);
                self.create(values, (err,result) =>{
                    if(err) {console.log(err); }
                    return callback(err,result)
                })

            }
    })
};

module.exports = mongoose.model('Usuario', usuarioSchema);