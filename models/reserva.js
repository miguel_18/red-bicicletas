var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var moment = require('moment');

var reservaSchema = new Schema({
    code:Number,
    desde: Date,
    hasta: Date,
    bicicleta: {type: mongoose.Schema.Types.ObjectId, ref: 'Bicicleta'},
    usuario: {type: mongoose.Schema.Types.ObjectId, ref: 'Usuario'}
});
reservaSchema.statics.createInstance = function(code, desde, hasta, bicicleta, usuario){
    return new this({
        code: code,
        desde: desde,
        hasta: hasta,
        bicicleta: bicicleta,
        usuario:usuario
    });
};
reservaSchema.methods.diasDeReserva = function () {
    return moment(this.hasta).diff(moment(this.desde), 'days') + 1;
}

module.exports = mongoose.model('Reserva', reservaSchema);